﻿EnableExplicit

UseGIFImageDecoder()
UseJPEG2000ImageDecoder()
UseJPEGImageDecoder()
UsePNGImageDecoder()
UseTGAImageDecoder()
UseTIFFImageDecoder()




UseCRC32Fingerprint()
IncludeFile #PB_Compiler_File + "i" ; PBHGEN

OpenConsole("BMP2CSV")

Define input_index.i = 0
Define format.i = #PB_UTF8
Define console_output.b = #True
Define skip_comma.i = 1
Define overwrite.b = #False
Define precede.s = ""
Define line_precede.s = ""
Define force24.b = #False

NewMap hex2setval.s()

While Left(ProgramParameter(input_index), 1) = "/"
  Select UCase(ProgramParameter(input_index))
    Case "/VERSION"
      PrintN("Version: 1.0")
      End
    Case "/ASCII"
      format = #PB_Ascii
    Case "/UTF8"
      format = #PB_UTF8
    Case "/UNICODE"
      format = #PB_Unicode
    Case "/OVERWRITE"
      overwrite = #True
    Case "/FORCE24"
      force24 = #True
    Case "/PRECEDE"
      precede = Trim(ProgramParameter(input_index+1))
      input_index = input_index + 1
    Case "/LINE_PRECEDE"
      line_precede = ProgramParameter(input_index+1)
      input_index = input_index + 1
    Case "/COMBINE"
      skip_comma = Val(Trim(ProgramParameter(input_index+1)))
      input_index = input_index + 1
    Case "/HEX2VALUE"
      AddMapElement(hex2setval(),Trim(ProgramParameter(input_index+1)))
      hex2setval() = Trim(ProgramParameter(input_index+2))
      input_index = input_index + 2
    Case "/HELP"
      PrintN("Converts an image file to hex values. Can also swap hex values for palette values (or any arbitrary values). Lots of options available. Can output to the console or to a text file / csv file. Check the readme for usage examples.")
      PrintN("")
      PrintN("BMP2CCcs [/HEX2VALUE value output_value] [/COMBINE value] [/LINE_PRECEDE "+Chr(34)+"value"+Chr(34)+"] [/PRECEDE value] [/HELP] [/VERSION] [/ASCII] [/UTF8] [/UNICODE] filename.bmp [filename.csv]") 
      PrintN("")
      PrintN("Available arguments:")
      PrintN("/PRECEDE - A string to put in front of every value.")
      PrintN("/LINE_PRECEDE - A string to put in front of every data line.")
      PrintN("/COMBINE - Skips n number of commas in between pixels. Useful for combining 2 or more pixels together before the next piece of data. Set to 0 for no separation. 1 is default.")
      PrintN("/VERSION - Displays program version.")
      PrintN("/HELP - Displays this help message.")
      PrintN("/HEX2VALUE - Converts the specified HEX value for a pixel to a palette number or any arbitrary value. Use this argument for every pixel value needed.")
      PrintN("/OVERWRITE - Overwrite output file if it already exists.")
      PrintN("/FORCE24 - Force 24-bit Mode.")
      PrintN("/ASCII - Output file is treated as ASCII file. Default is to read all files as UTF8 files.")
      PrintN("/UTF8 - Output file is treated as UTF8 file. Default is to read all files as UTF8 files.")
      PrintN("/UNICODE - Output file is treated as Unicode (UCS2) file. Default is to read all files as UTF8 files.")
      PrintN("filename.bmp - Input file, can be full path to filename. Supported image formats are bmp, jpg, tiff, png, gif, tga. If one format does not work, try another format or re-export your image.")
      PrintN("filename.csv - Output file, can be full path to filename. Will prompt to overwrite files unless overridden. If no file is provided, output is sent to console.")
      PrintN("")
    Default
      PrintN("Invalid switch: " + ProgramParameter(input_index))
  EndSelect
  input_index = input_index + 1
Wend

Define input.s = Trim(ProgramParameter(input_index))
Define output.s = Trim(ProgramParameter(input_index + 1))
Define input_file.i
Define output_file.i


If input = ""
  PrintN("Unable to process. No input provided.")
  End
EndIf

input_file = LoadImage(#PB_Any, input, 0)
If input_file = 0
  PrintN("Unable to open file for reading: " + input)
  End
EndIf



If output <> ""
  console_output = #False
  If FileSize(output) > -1 And overwrite = #False
    Print("Overwrite existing file? [Y,n] ")
    Define overwrite_prompt.s = Input()
    If LCase(overwrite_prompt) <> "y"
      PrintN("Unable to process. File already exists. To overwrite file, use /OVERWRITE switch: " + output)
      End
    EndIf
  EndIf

  output_file = CreateFile(#PB_Any, output, #PB_File_SharedWrite | format)
  If output_file = 0
    PrintN("Unable to process. Error creating file: " + output)
    End
  EndIf
  
EndIf

Define img_width.i = ImageWidth(input_file)
Define img_height.i = ImageHeight(input_file)
Define img_depth.i = ImageDepth(input_file, #PB_Image_InternalDepth)
Define img_layers.i = ImageFrameCount(input_file)

StartDrawing(ImageOutput(input_file))

Define x.i
Define y.i
Define layer.i
Define combine_count.i = 1

If console_output = #False
  WriteStringN(output_file, GetFilePart(input, #PB_FileSystem_NoExtension))
Else
  PrintN(GetFilePart(input, #PB_FileSystem_NoExtension))
EndIf
 

For layer = 0 To img_layers-1
  SetImageFrame(input_file, layer)
  For y = 0 To img_height-1
    If console_output = #False
      WriteString(output_file, line_precede)
    Else
      Print(line_precede)
    EndIf
    If console_output = #False
      WriteString(output_file, precede)
    Else
      Print(precede)
    EndIf
    For x = 0 To img_width-1
      Define value.s
      Define map_value.i
      
      If combine_count <> skip_comma
        combine_count = combine_count + 1
      Else
        combine_count = 1
      EndIf
         
      If img_depth = 32 And force24 = #False
        value = RSet(Hex(Red(Point(x,y))),2,"0") + RSet(Hex(Green(Point(x,y))),2,"0") + RSet(Hex(Blue(Point(x,y))),2,"0") + RSet(Hex(Alpha(Point(x,y))),2,"0")
      Else
        value = RSet(Hex(Red(Point(x,y))),2,"0") + RSet(Hex(Green(Point(x,y))),2,"0") + RSet(Hex(Blue(Point(x,y))),2,"0")
      EndIf

      
      map_value = FindMapElement(hex2setval(),value)
      If map_value
        value = hex2setval(value)
      EndIf      
      
      If console_output = #False     
        WriteString(output_file,value)
        If x < img_width -1
          If combine_count = 1
            WriteString(output_file,"," + precede)
          EndIf
        EndIf
      Else
        Print(value)
        If x < img_width -1
          If combine_count = 1
            Print(","+precede)
          EndIf
        EndIf
      EndIf
    Next
    If console_output = #False
      WriteStringN(output_file, "")
    Else
      PrintN("")
    EndIf
  Next
  If img_layers > 1
    If console_output = #False
      WriteStringN(output_file,"")
    Else
      PrintN("")
    EndIf
  EndIf

Next layer


CloseFile(output_file)
CloseConsole()


Procedure.q Hex2Dec(Hex.s)
   Protected result.q, n, temp, pow.q=1
   hex=UCase(hex)
   For n=MemoryStringLength(@Hex)-1 To 0 Step -1
      temp=PeekC(@Hex+n*SizeOf(Character))-48
      If temp >= 17 And temp <= 22
         temp-7
      ElseIf temp < 0 Or temp > 9
         Break
      EndIf
      result+temp*pow
      pow*16
   Next
   ProcedureReturn result
 EndProcedure
; IDE Options = PureBasic 5.73 LTS (Windows - x64)
; CursorPosition = 59
; FirstLine = 45
; Folding = -
; EnableXP